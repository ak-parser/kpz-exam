﻿namespace react_asp.Entities
{
    public partial class Response
    {
        public int Id { get; set; }
        public string Comment { get; set; } = null!;
        public bool IsRecommend { get; set; }
        public int FilmId { get; set; }

        public virtual Film Film { get; set; }
    }
}
