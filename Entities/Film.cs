﻿namespace react_asp.Entities
{
    public partial class Film
    {
        public Film()
        {
            Responses = new HashSet<Response>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Url { get; set; } = null!;
        public bool IsRecommend { get; set; }
        public int Rating { get; set; }

        public virtual ICollection<Response> Responses { get; set; }
    }
}
