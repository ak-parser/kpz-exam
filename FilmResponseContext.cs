﻿using Microsoft.EntityFrameworkCore;
using react_asp.Entities;

namespace react_asp
{
    public partial class FilmResponseContext : DbContext
    {
        public FilmResponseContext()
        {
        }

        public FilmResponseContext(DbContextOptions<FilmResponseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Film> Films { get; set; } = null!;
        public virtual DbSet<Response> Responses { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=FilmResponse;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Film>(entity =>
            {
                entity.ToTable("Film");
            });

            modelBuilder.Entity<Response>(entity =>
            {
                entity.HasIndex(e => e.FilmId, "IX_Responses_FilmId");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.Responses)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Response>().Navigation(elem => elem.Film).AutoInclude();
            modelBuilder.Entity<Film>().Navigation(elem => elem.Responses).AutoInclude();

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
