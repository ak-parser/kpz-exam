﻿#nullable disable
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using react_asp.Entities;
using react_asp.Repository;

namespace react_asp.Controllers
{
    [ApiController]
    [Route("api/response")]
    public class ResponseController : ControllerBase
    {
        private readonly IRepository<Response> _repository;
        private readonly IMapper _mapper;

        public ResponseController(IRepository<Response> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Response
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> GetAll()
        {
            var items = await _repository.GetList();

            var models = items.Select(item => _mapper.Map<ResponseModel>(item));

            return Ok(models);
        }

        // GET: api/Response/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ResponseModel>> Get(int id)
        {
            var item = await _repository.GetItem(id);

            if (item == null)
                return NotFound();

            return Ok(_mapper.Map<ResponseModel>(item));
        }

        // PUT: api/Response/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, ResponseModel model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }

            _repository.Update(_mapper.Map<Response>(model));

            try
            {
                await _repository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(await Exists(id)))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Response
        [HttpPost]
        public async Task<ActionResult<ResponseModel>> Post(ResponseModel model)
        {
            if (model == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                var item = _mapper.Map<Response>(model);

                _repository.Create(item);
                await _repository.Save();

                return CreatedAtAction("GetProduct", new { id = model.Id }, _mapper.Map<ResponseModel>(item));
            }
            return ValidationProblem();
        }

        // DELETE: api/Response/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _repository.GetItem(id);
            if (response == null)
            {
                return NotFound();
            }

            _repository.Delete(response);
            await _repository.Save();

            return NoContent();
        }

        private async Task<bool> Exists(int id)
        {
            return await _repository.EntityExists(id);
        }
    }
}
