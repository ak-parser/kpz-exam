﻿#nullable disable
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using react_asp.Entities;
using react_asp.Repository;

namespace react_asp.Controllers
{
    [ApiController]
    [Route("api/film")]
    public class FilmController : ControllerBase
    {
        private readonly IRepository<Film> _repository;
        private readonly IMapper _mapper;

        public FilmController(IRepository<Film> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/film
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FilmModel>>> GetAll()
        {
            var items = await _repository.GetList();

            var models = items.Select(item => _mapper.Map<FilmModel>(item));

            return Ok(models);
        }

        // GET: api/film/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FilmModel>> Get(int id)
        {
            var item = await _repository.GetItem(id);

            if (item == null)
                return NotFound();

            return Ok(_mapper.Map<FilmModel>(item));
        }

        // PUT: api/film/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, FilmModel model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }

            _repository.Update(_mapper.Map<Film>(model));

            try
            {
                await _repository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(await Exists(id)))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/film
        [HttpPost]
        public async Task<ActionResult<FilmModel>> Post(FilmModel model)
        {
            if (model == null)
                return NotFound();


            if (ModelState.IsValid)
            {
                var item = _mapper.Map<Film>(model);
                
                _repository.Create(item);
                await _repository.Save();

                return CreatedAtAction("Get", new { id = model.Id }, _mapper.Map<FilmModel>(item));
            }
            return ValidationProblem();
        }

        // DELETE: api/film/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _repository.GetItem(id);
            if (response == null)
            {
                return NotFound();
            }

            _repository.Delete(response);
            await _repository.Save();

            return NoContent();
        }

        private async Task<bool> Exists(int id)
        {
            return await _repository.EntityExists(id);
        }
    }
}
