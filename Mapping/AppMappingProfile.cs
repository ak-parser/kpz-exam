﻿using AutoMapper;
using react_asp.Entities;

namespace react_asp.Mapping
{
    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<Response, ResponseModel>().ReverseMap();
            CreateMap<Film, FilmModel>().ReverseMap();
        }
    }
}
