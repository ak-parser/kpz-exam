﻿namespace react_asp.Repository
{
    public interface IRepository<TEntity>
    {
        public Task<List<TEntity>> GetList();
        public Task<TEntity?> GetItem(int id);
        public void Create(TEntity item);
        public void Update(TEntity item);
        public void Delete(TEntity product);
        public Task<int> Save();
        public Task<bool> EntityExists(int id);
    }
}