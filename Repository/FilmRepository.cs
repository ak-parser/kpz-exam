﻿using Microsoft.EntityFrameworkCore;
using react_asp.Entities;

namespace react_asp.Repository
{
    public class FilmRepository : IRepository<Film>
    {
        private FilmResponseContext _dBContext;
        public FilmRepository()
        {
            _dBContext = new FilmResponseContext();
        }

        public async void Create(Film item)
        {
            await _dBContext.Films.AddAsync(item);
        }

        public void Delete(Film item)
        {
            _dBContext.Remove(item);
        }

        public async Task<Film?> GetItem(int id)
        {
            return await _dBContext.Films.FindAsync(id);
        }

        public async Task<List<Film>> GetList()
        {
            return await _dBContext.Films.ToListAsync();
        }

        public async Task<int> Save()
        {
            return await _dBContext.SaveChangesAsync();
        }

        public void Update(Film item)
        {
            _dBContext.Entry(item).State = EntityState.Modified;
        }

        public async Task<bool> EntityExists(int id)
        {
            return await _dBContext.Films.AnyAsync(elem => elem.Id == id);
        }
    }
}
