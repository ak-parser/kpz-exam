﻿using Microsoft.EntityFrameworkCore;
using react_asp.Entities;

namespace react_asp.Repository
{
    public class ResponseRepository : IRepository<Response>
    {
        private FilmResponseContext _dBContext;
        public ResponseRepository()
        {
            _dBContext = new FilmResponseContext();
        }

        public async void Create(Response item)
        {
            await _dBContext.Responses.AddAsync(item);
        }

        public void Delete(Response item)
        {
            _dBContext.Remove(item);
        }

        public async Task<Response?> GetItem(int id)
        {
            return await _dBContext.Responses.FindAsync(id);
        }

        public async Task<List<Response>> GetList()
        {
            return await _dBContext.Responses.ToListAsync();
        }

        public async Task<int> Save()
        {
            return await _dBContext.SaveChangesAsync();
        }

        public void Update(Response item)
        {
            _dBContext.Entry(item).State = EntityState.Modified;
        }

        public async Task<bool> EntityExists(int id)
        {
            return await _dBContext.Responses.AnyAsync(elem => elem.Id == id);
        }
    }
}
