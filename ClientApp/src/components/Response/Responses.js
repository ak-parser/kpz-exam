import React, { Component } from 'react';
import {AddResponse} from './AddResponse';
import {DeleteResponse} from './DeleteResponse';
import {ChangeResponse} from './ChangeResponse';

import '../Manager.css';

export class Responses extends Component {
    constructor(props) {
        super(props);
        this.state = { responses: [], loading: true};
    }

    componentDidMount() {
        this.populateResponsesData();
    }

    static renderResponsesTable(responses, populateResponsesData) {
        return (
            <table className='table table-striped table-hover' aria-labelledby="tableLabel">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Comment</th>
                    <th>Recommend</th>
                    <th>Film ID</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                {responses.map((response) =>
                    <tr key={response.id}>
                        <td>{response.id}</td>
                        <td>{response.comment}</td>
                        <td>{response.isRecommend ? "Yes" : "No"}</td>
                        <td>{response.filmId}</td>
                        <td className="table-operation">
                            <div>
                                <DeleteResponse id={response.id} onUpdate={populateResponsesData}>{}</DeleteResponse>
                                <ChangeResponse response={response} onUpdate={populateResponsesData}>{}</ChangeResponse>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Responses.renderResponsesTable(this.state.responses, () => this.populateResponsesData());

        return (
            <main>
                <section className="header">
                    <h1 id="tableLabel">Responses</h1>
                    <AddResponse onUpdate={() => this.populateResponsesData()}>{}</AddResponse>
                </section>
                {contents}
            </main>
        );
    }

    async populateResponsesData() {
        const response = await fetch('api/response');
        const data = await response.json();
        this.setState({ responses: data, loading: false });
    }
}
