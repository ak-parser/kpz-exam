import React, {useState} from "react";
import {ModalWindow} from "../ModalWindow";

import '../Operations.css'

export  function AddResponse(props) {
    const [modal, setModal] = useState(false);

    const onSubmit = async (event) => {
        event.preventDefault();
        setModal(!modal);

        const url = 'api/response';
        const form = document.forms['response'];
        const responseToSend = {
            comment: form['comment'].value,
            isRecommend: form['isRecommend'].checked,
            filmId: form['filmId'].value
        };

        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(responseToSend),
            headers: {'Content-type': 'application/json'}
        });
        props.onUpdate();
    }

    const onSetShowModal = () => {
        setModal(!modal);
    }

    return (
        <>
            <button className="modal-button-add" onClick={onSetShowModal}>{}</button>
            <ModalWindow show={modal} onHide={onSetShowModal} header={"Add response"}>
                <form name="response" autoComplete onSubmit={(event) => onSubmit(event)}>
                    <label>
                        Comment
                        <input name="comment" type="text" required minLength="1" placeholder="Good"/>
                    </label>
                    <label>
                        Recommend
                        <input name="isRecommend" type="checkbox"/>
                    </label>
                    <label>
                        Film ID
                        <input name="filmId" type="number" required min="0" step="1" placeholder="1"/>
                    </label>
                    <button type="submit">Add</button>
                </form>
            </ModalWindow>
        </>
    )
}