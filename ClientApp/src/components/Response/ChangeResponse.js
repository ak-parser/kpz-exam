import React, {useState} from "react";
import {ModalWindow} from "../ModalWindow";
import '../Operations.css'

export  function ChangeResponse(props) {
    const [modal, setModal] = useState(false);
    const [comment, setComment] = useState(props.response.comment);
    const [isRecommend, setIsRecommend] = useState(props.response.isRecommend);
    const [filmId, setFilmId] = useState(props.response.filmId);

    const onSubmit = async (event) => {
        event.preventDefault();
        setModal(!modal);

        const url = 'api/response/' + props.response.id;
        const responseToSend = {
            'id': props.response.id,
            comment,
            isRecommend,
            filmId,
        };

        await fetch(url, {
            method: 'PUT',
            body: JSON.stringify(responseToSend),
            headers: {'Content-type': 'application/json'}
        });
        props.onUpdate();
    }

    const onSetShowModal = () => {
        setModal(!modal);
    }

    return (
        <>
            <img src="/change-icon.png" onClick={onSetShowModal} alt="Change"/>
            <ModalWindow show={modal} onHide={onSetShowModal} header={"Change response"}>
                <form name="response" autoComplete onSubmit={(event) => onSubmit(event)}>
                    <label>
                        Comment
                        <input value={comment} name="comment" type="text" required minLength="1" placeholder="Good" onChange={(e) => setComment(e.target.value)}/>
                    </label>
                    <label>
                        Recommend
                        <input value={isRecommend} name="isRecommend" type="checkbox" onChange={(e) => setIsRecommend(e.target.checked)}/>
                    </label>
                    <label>
                        Film ID
                        <input value={filmId} name="filmId" type="number" required min="0" step="1" placeholder="1" onChange={(e) => setFilmId(e.target.value)}/>
                    </label>
                    <button type="submit">Change</button>
                </form>
            </ModalWindow>
        </>
    )
}