import React, {useState} from "react";
import {ModalWindow} from "../ModalWindow";
import '../Operations.css'

export  function ChangeFilm(props) {
    const [modal, setModal] = useState(false);

    const onSubmit = async (event) => {
        event.preventDefault();
        setModal(!modal);

        const url = 'api/film/' + props.film.id;
        const form = document.forms['film'];
        const filmToSend = {
            id: props.film.id,
            name: form['name'].value,
            url: form['url'].value,
            isRecommend: form['isRecommend'].checked,
            rating: form['rating'].value,
        };

        await fetch(url, {
            method: 'PUT',
            body: JSON.stringify(filmToSend),
            headers: {'Content-type': 'application/json'}
        });
        props.onUpdate();
    }

    const onSetShowModal = () => {
        setModal(!modal);
    }

    return (
        <>
            <img src="/change-icon.png" onClick={onSetShowModal} alt="Change"/>
            <ModalWindow show={modal} onHide={onSetShowModal} header={"Change film"}>
                <form name="film" autoComplete onSubmit={(event) => onSubmit(event)}>
                    <label>
                        Name
                        <input name="name" type="text" required/>
                    </label>
                    <label>
                        Url
                        <input name="url" type="text" required/>
                    </label>
                    <label>
                        Recommend
                        <input name="isRecommend" type="checkbox"/>
                    </label>
                    <label>
                        Rating
                        <input name="rating" type="number" required min="0" max="5"/>
                    </label>
                    <button type="submit">Change</button>
                </form>
            </ModalWindow>
        </>
    )
}