import React, { Component } from 'react';
import '../Manager.css'
import {AddFilm} from "./AddFilm";
import {DeleteFilm} from "./DeleteFilm";
import {ChangeFilm} from "./ChangeFilm";
import {Responses} from '../Response/Responses'
import {Accordion} from "react-bootstrap";

export class Films extends Component {
    constructor(props) {
        super(props);
        this.state = { films: [], loading: true };
    }

    componentDidMount() {
        this.populateFilmsData();
    }

    static renderFilmsTable(films, populateFilmsData) {
        return (
            <Accordion>
            <table className='table table-striped table-hover' aria-labelledby="tableLabel">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Url</th>
                    <th>Recommend</th>
                    <th>Rating</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                {films.map((film) =>
                    <>
                        <tr key={film.id}>
                            <td>{film.id}</td>
                            <td>{film.name}</td>
                            <td><a href={film.url}>Link</a></td>
                            <td>{film.isRecommend ? "Yes" : "No"}</td>
                            <td>{film.rating}</td>
                            <td className="table-operation">
                                <div>
                                    <DeleteFilm id={film.id} onUpdate={populateFilmsData}>{}</DeleteFilm>
                                    <ChangeFilm film={film} onUpdate={populateFilmsData}>{}</ChangeFilm>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan={6}>
                                <Accordion.Item eventKey={film.id}>
                                    <Accordion.Header style={{padding: "0"}}>Responses</Accordion.Header>
                                    <Accordion.Body>
                                        {Responses.renderResponsesTable(film.responses, populateFilmsData)}
                                    </Accordion.Body>
                                </Accordion.Item>
                            </td>
                        </tr>
                    </>
                )}
                </tbody>
            </table>
            </Accordion>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Films.renderFilmsTable(this.state.films, () => this.populateFilmsData());

        return (
            <main>
                <section className="header">
                    <h1 id="tableLabel" >Films</h1>
                    <AddFilm onUpdate={() => this.populateFilmsData()}>{}</AddFilm>
                </section>
                {contents}
            </main>
        );
    }

    async populateFilmsData() {
        const response = await fetch('api/film');
        const data = await response.json();
        this.setState({ films: data, loading: false });
    }
}
