import React, { Component } from 'react';
import { Route, Routes } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Responses } from './components/Response/Responses';
import { Films } from './components/Film/Films';

import './custom.css';

export default class App extends Component {
  render () {
    return (
      <Layout>
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route path='/films' element={<Films />} />
          <Route path='/responses' element={<Responses />} />
        </Routes>
      </Layout>
    );
  }
}
