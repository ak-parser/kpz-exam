﻿namespace react_asp.Entities
{
    public class ResponseModel
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public bool IsRecommend { get; set; }
        public int FilmId { get; set; }
    }
}
