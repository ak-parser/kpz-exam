﻿namespace react_asp.Entities
{
    public class FilmModel
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsRecommend { get; set; }
        public int Rating { get; set; }

        public ICollection<ResponseModel> Responses { get; set; } = new HashSet<ResponseModel>();
    }
}
